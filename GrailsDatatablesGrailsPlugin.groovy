class GrailsDatatablesGrailsPlugin {
    // the plugin version
    def version = "0.16"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "2.5 > *"
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
        "grails-app/views/error.gsp"
    ]

    // TODO Fill in these fields
    def title = "Grails DataTables Plugin" // Headline display name of the plugin
    def author = "Ben Wilson"
    def authorEmail = "ben.wilson@icsynergy.com"
    def description = '''\
This plugin allows you to quickly add feature-rich tables to your Grails application. It uses the excellent DataTables plugin for jQuery created by SpryMedia Ltd.

This plugin provides the following components to your application:
- A taglib that you use to define the table in your GSP. The taglib generates all the HTML and JavaScript necessary to create the table.
- A controller and services that provide data for the table.
- An extensible reporting system.
'''

    // URL to the plugin's documentation
    def documentation = "https://bitbucket.org/ben-wilson/grails-datatables/wiki/Home"

    // Extra (optional) plugin metadata

    // License: one of 'APACHE', 'GPL2', 'GPL3'
    def license = "APACHE"

    // Location of the plugin's issue tracker.
    def issueManagement = [ system: "BITBUCKET", url: "https://bitbucket.org/ben-wilson/grails-datatables/issues" ]

    // Online location of the plugin's browseable source code.
    def scm = [ url: "https://bitbucket.org/ben-wilson/grails-datatables/src" ]
}
